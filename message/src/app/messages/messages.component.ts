import { Component, OnInit } from '@angular/core';
import { MessageService } from './message.service'; 

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  messages;
  messagesKeys;

  constructor(private service:MessageService) { 
    service.getMessages().subscribe(
      response=>{
        this.messages = response.json();
        this.messagesKeys = Object.keys(this.messages);
      }

    );
  }

  ngOnInit() {

  }

}
